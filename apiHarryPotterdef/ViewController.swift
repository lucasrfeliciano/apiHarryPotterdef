//
//  ViewController.swift
//  apiHarryPotterdef
//
//  Created by COTEMIG on 03/11/22.
//

import UIKit
import Alamofire
import Kingfisher

struct Ator:Decodable {
    let image : String
    let actor : String
    let name : String
}

class ViewController: UIViewController, UITableViewDataSource {

    var listaDeAtor:[Ator] = []
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.listaDeAtor.count
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Minha Celula", for: indexPath) as! MyCell
        let ator = self.listaDeAtor[indexPath.row]
        
        cell.nome_ator.text = ator.actor
        cell.nome_personagem.text = ator.name
        cell.img_ator.image = UIImage(named: ator.image)
        cell.img_ator.kf.setImage(with: URL(string: ator.image))
        return cell
    }
    @IBOutlet weak var tableview: UITableView!
    override func viewDidLoad() {
        self.tableview.dataSource = self
        super.viewDidLoad()
        getAtor()
        
    }
    
    func getAtor() {
        AF.request("https://hp-api.herokuapp.com/api/characters").responseDecodable(of: [Ator].self){
            response in
            if let ator = response.value{
                self.listaDeAtor = ator
                print(self.listaDeAtor)
            }
            self.tableview.reloadData()
        }

    }
}

    

