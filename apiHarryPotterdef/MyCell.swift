//
//  MyCell.swift
//  apiHarryPotterdef
//
//  Created by COTEMIG on 03/11/22.
//

import UIKit

class MyCell: UITableViewCell {

    @IBOutlet weak var img_ator: UIImageView!
    @IBOutlet weak var nome_personagem: UILabel!
    @IBOutlet weak var nome_ator: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
